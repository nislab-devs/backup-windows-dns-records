# Backup Windows DNS records

This PowerShell script will export Windows DNS records to CSV files. The record types that are currently supported are:
- A
- AAAA
- CNAME

## Usage

The script content can be cloned/copied/downloaded and executed as a separate PowerShell (.ps1) file.

### Variables

The following variables are required:
- `$ZoneName`: The Forward DNS Zone that contains the records
- `$BackupPath`: The absolute path to the directory that the CSV files will be exported to

### Single-line execution

Single-line execution can be achieved using the following methods:

**Using PowerShell**

```powershell
powershell.exe -Command {$ZoneName = 'your.dns.zone'; $BackupPath = 'C:\your\backup\dir\'; Invoke-Expression $(Invoke-WebRequest https://gitlab.com/nislab-devs/backup-windows-dns-records/-/raw/main/backup-records.ps1).Content}
```

**Using Command Prompt**

```powershell
powershell.exe -Command "& {$ZoneName = 'your.dns.zone'; $BackupPath = 'C:\your\backup\dir\'; Invoke-Expression $(Invoke-WebRequest https://gitlab.com/nislab-devs/backup-windows-dns-records/-/raw/main/backup-records.ps1).Content}"
```

### Exported fields

For each record type, the fields that the script will include in the CSV files are:

| Record Type | Fields |
| ---         | ---    |
| A           | `HostName`: The record's name<br>`RecordType`: The record type<br>`TimeToLive`: The record's TTL<br>`RecordData`: The IPv4 address for the record |
| AAAA        | `HostName`: The record's name<br>`RecordType`: The record type<br>`TimeToLive`: The record's TTL<br>`RecordData`: The IPv6 address for the record |
| CNAME       | `HostName`: The alias name<br>`RecordType`: The record type<br>`TimeToLive`: The record's TTL<br>`RecordData`: The Fully-qualified Domain Name (FQDN) of the A/AAAA record being pointed to |
